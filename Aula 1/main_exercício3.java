package application;

public class Principal {

	public static void main(String[] args) {
		Membros membro1 = new Membros("Carol", 24, 2, "SEK");
		
		membro1.dadosMembros();

		membro1.setMembros(25,3);
		membro1.dadosMembros();
		
		membro1.setMembros("Carolina", 3);
		membro1.dadosMembros();
		
		membro1.setMembros(25, 3, "Humanoide");
		membro1.dadosMembros();
		
	}	
	
	
	
}
