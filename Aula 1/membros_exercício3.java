package application;

public class Membros {
	
	 private String nome ;
	 private String segmentoDaEquipe ;
	 private int idade ;
	 private int tempoDeServico;
	
	
	 public Membros(String name, int age, int time, String seg) {

		    this.setNome (name);
	        this.setIdade (age);
	        this.setTempoDeServico (time);
	        this.setSegmento (seg);
	 	}
	 public void setMembros(String name, int age, int time, String seg) {
			this.nome=name;
			this.idade=age;
			this.tempoDeServico=time;
			this.segmentoDaEquipe=seg;
		}
		
		public void setMembros( int age, int time, String seg) {
			this.idade=age;
			this.tempoDeServico=time;
			this.segmentoDaEquipe=seg;
		}
		
		public void setMembros(String name, int age, int time) {
			this.nome=name;
			this.idade=age;
			this.tempoDeServico=time;
		}
		
		public void setMembros(String name, int time, String seg) {
			this.nome=name;
			this.tempoDeServico=time;
			this.segmentoDaEquipe=seg;
		}
		
		public void setMembros(String name, int time) {
			this.nome=name;
			this.tempoDeServico=time;
		}
		
		public void setMembros(int time, String seg) {
			this.tempoDeServico=time;
			this.segmentoDaEquipe=seg;
		}
		
		public void setMembros(int age, int time) {
			this.idade=age;
			this.tempoDeServico=time;
		}
		
		public void setMembros(String name, String seg) {
			this.nome=name;
			this.segmentoDaEquipe=seg;
		}
	 
	  public void setNome (String name) {
	        this.nome = name;
	    }

	    public String getNome() {
	        return this.nome;
	    }

	    public void setIdade (int age) {
	        this.idade = age;
	    }

	    public int getIdade () {
	        return this.idade;
	    }
	    
	    public void setTempoDeServico (int time) {
	        this.tempoDeServico = time;
	    }

	    public int getTempoDeServico() {
	        return this.tempoDeServico;
	    }

	    public void setSegmento (String seg) {
	        this.segmentoDaEquipe = seg;
	    }

	    public String getSegmento () {
	        return this.segmentoDaEquipe;
	    }


	    void dadosMembros () {
	    	System.out.println(nome);
	    	System.out.println(idade);
	    	System.out.println(segmentoDaEquipe);
	    	System.out.println(tempoDeServico);
	    	System.out.println();
	    }
}
