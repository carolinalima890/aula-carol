package exercicio8;

public class Soma {
	
	private static int[][] matrizSoma = new int[3][3];
	
	public static void Somar() {
		
		Matriz.matrizInalterável();
		Matriz.matrizAleatoria();
		
		Matriz.printMatrizInalteravel();
		Matriz.printMatrizAleatoria();
		System.out.println();
		System.out.println("Matriz soma: ");
		
		for(int i=0; i<matrizSoma.length; i++) {
			for(int j=0; j<matrizSoma.length; j++) {
				matrizSoma[i][j] = Matriz.getMatriz1()[i][j] + Matriz.getMatriz2()[i][j];
				
				System.out.print(matrizSoma[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
}
