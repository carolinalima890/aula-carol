package exercicio8;

public class Matriz {

	private static int [][] matriz2 = new int[3][3];
	private static int [][] matriz1 = new int[3][3];

	public static void matrizInalterável() {
		for (int i=0; i<matriz1.length; i++) {
			for(int j=0; j<matriz1.length; j++) {
				matriz1 [i][j]=2*i+j;
			}	
		}
	}
	
	public static void printMatrizInalteravel() {
		System.out.println();
		System.out.println("Matriz Inalterável: ");
		for (int i=0; i<matriz1.length; i++) {
			for(int j=0; j<matriz1[i].length; j++) {
				System.out.print(matriz1[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public static void matrizAleatoria() {
		for (int i=0; i<matriz2.length; i++) {
			for(int j=0; j<matriz2.length; j++) {
				double a= Math.random()*10;
				matriz2[i][j]= (int) a;
			}
		}
	}
	
	public static void printMatrizAleatoria() {
		System.out.println();
		System.out.println("Matriz Aleatória: ");
		for (int i=0; i<matriz2.length; i++) {
			for(int j=0; j<matriz2[i].length; j++) {
				System.out.print(matriz2[i][j] + " ");
			}
			System.out.println();
		}	
		System.out.println();
	}
	
	public static int [][] getMatriz1() {
		return matriz1;
	}

	public static int [][] getMatriz2() {
		return matriz2;
	}
	public static void setMatriz2(int [][] matriz2) {
		Matriz.matriz2 = matriz2;
	}

	
}

