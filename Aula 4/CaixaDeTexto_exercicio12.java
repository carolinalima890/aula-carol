package exercicio12;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class CaixaDeTexto extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField usuario;
	private JPasswordField senha;
	private JButton login, limpa;
	private JLabel user, pass;

	public CaixaDeTexto(){
		super("Login");
		setLayout(new FlowLayout());

		user = new JLabel("Usu�rio: ");
		add(user);

		usuario = new JTextField(15);
		add(usuario);

		pass = new JLabel("Senha:   ");
		add(pass);

		senha = new JPasswordField(15);
		add(senha);

		login = new JButton("Entrar");
		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evento){
				if(evento.getSource() == login)
					if(usuario.getText().equals("Java")  )
						JOptionPane.showMessageDialog(null, "Sucesso no Login!");
					else
						JOptionPane.showMessageDialog(null, "Senha incorreta");

			}
		}
				);
		add(login);

		limpa = new JButton("Limpar");
		limpa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evento){
				if(evento.getSource() == limpa){
					usuario.setText("");
					senha.setText("");
				}
			}
		}
				);
		add(limpa);
	}

}
