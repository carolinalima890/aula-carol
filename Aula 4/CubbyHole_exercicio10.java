package exercicio10;

public class CubbyHole {

	private int contents;
	private boolean available = false;

	public synchronized int get(int number) {

		while (available == false) {
			try {
				wait();
			} catch (InterruptedException e) { }
		}
		available = false;
		notifyAll();
		return contents;
	}

	public synchronized void put(int value, int i) {
		while (available == true) {
			try {
				wait();
			} catch (InterruptedException e) { }
		}
		contents = value;
		available = true;

		notifyAll();
	}
}
