package exercicio10;

public class ThreadProdutora extends Thread {
	
	private CubbyHole cubbyhole;
	private int number;

	public ThreadProdutora(CubbyHole c, int number) {
		cubbyhole = c;
		this.number = number;
	}

	public void run() {
		for (int i = 0; i < 10; i++) {
			cubbyhole.put(number, i);
			System.out.println();
			try {
				sleep((int)(Math.random() * 100));
			} catch (InterruptedException e) { }
		}
	}
}


